(function() {
	'use strict';

	angular
		.module('lily')
		.config(routerConfig);

	/** @ngInject */
	function routerConfig($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('dashboard', {
				url: '/',
				templateUrl: 'app/pages/dashboard/dashboard.html',
				controller: 'DashboardController',
				controllerAs: 'dashboard'
			});

		$urlRouterProvider.otherwise('/');
	}

})();
