(function() {
	'use strict';


	/**
	 * html Filter
	 *
	 * Description
	 */
	angular.module('utils')
		.filter('html', function($sce) {
			return function(input) {
				return $sce.trustAsHtml(input.toString());
			};
		});
})();
