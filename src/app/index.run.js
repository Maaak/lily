(function() {
	'use strict';

	angular
		.module('lily')
		.run(runBlock);

	/** @ngInject */
	function runBlock($log) {

		$log.debug('runBlock end');
	}

})();
