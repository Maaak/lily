(function() {
	'use strict';

	angular
		.module('lily')
		.config(config);

	/** @ngInject */
	function config($logProvider) {
		// Enable log
		$logProvider.debugEnabled(true);
	}

})();
