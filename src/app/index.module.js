(function() {
	'use strict';

	angular
		.module('lily', [
			'ui.router',
			'ngMaterial',
			'toastr',
			'ngMap',

			'utils',

			'pages'
		]);

})();
