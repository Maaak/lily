(function(){
	'use strict';

	/**
	 * CityEventService
	 *
	 * Description
	 */

	angular
		.module('cityEvent')
		.service('CityEventService', CityEventService);

	/** @ngInject */
	function CityEventService($http, API) {

		var service = {
			getEventsList: 	getEventsList
		};
		return service;

		function getEventsList () {
			return $http.get(API.eventsList).then(function(response) {
				return response.data.event.map(function(event) {
					return {
						id: event.id,
						lang: event.lang,
						title: event.label[0].value,
						description: event.description[1].value,
						position: event.location.point[0].Point.posList.split(' ').join(','),
						link: event.link[0].href
					};
				});
			}, function(reject) {
				return reject;
			});
		}
	}
})();