(function() {
	'use strict';


	/**
	* moduleName Module
	*
	* Description
	*/
	angular
		.module('cityEvent')
		.constant('API', API());

		function API () {
			return {
				eventsList: 'http://citysdk.dmci.hva.nl/CitySDK/events/search?category=festival'
			}
		}
})();