(function() {
	'use strict';


	/**
	 * List Directive
	 *
	 * Description
	 */
	angular
		.module('list')
		.directive('list', List);


	/** @ngInject */
	function List() {
		return {
			// name: '',
			// priority: 1,
			// terminal: true,
			scope: {}, // {} = isolate, true = child, false/undefined = no change
			bindToController: {
				items: '='
			},
			controller: 'ListController',
			controllerAs: 'list',
			// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
			// restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
			// template: '',
			templateUrl: 'app/components/list/list.html'
			// replace: true,
			// transclude: true,
			// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),

			/* jshint unused: false */
			// link: function($scope, el, attr, controller) {}
		};
	}
})();