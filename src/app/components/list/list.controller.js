(function() {
	'use strict';

	/**
	 * ListController
	 *
	 * Description
	 */

	angular
		.module('list')
		.controller('ListController', ListController);

	/** @ngInject */
	function ListController($scope) {
		var vm = this;

		vm.onItemClick = function(item) {
			$scope.$emit('item:clicked', item);
		}
	}
})();
