(function() {
	'use strict';

	/**
	 * MapController
	 *
	 * Description
	 */

	angular
		.module('map')
		.controller('MapController', MapController);

	/** @ngInject */
	function MapController($rootScope, $scope, NgMap) {

		var i;
		var pos;
		var bounds;

		var vm = this;
		vm.positions = [];

		var mapInitWatcher = $rootScope.$on('mapInitialized', function() {
			bounds = new google.maps.LatLngBounds();

			for (i = 0; i < vm.positions.length; i++) {
				pos = vm.positions[i].split(',');
				bounds.extend(new google.maps.LatLng(pos[0], pos[1]));
			}

			NgMap.getMap().then(function(map) {
				map.setCenter(bounds.getCenter());
				map.fitBounds(bounds);
				map.setZoom(map.getZoom() - 2);
			});

		});


		var markersWatcher = $scope.$watch(function() {
			return vm.markers;
		}, function(newValue) {
			if (newValue) {
				vm.positions = newValue.map(function(pos) {
					return pos.position;
				});
			}
		});

		$scope.$on('$destroy', function() {
			markersWatcher();
			mapInitWatcher();
		});
	}
})();
