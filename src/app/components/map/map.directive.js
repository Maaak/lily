(function() {
	'use strict';


	/**
	 * Map Directive
	 *
	 * Description
	 */
	angular
		.module('map')
		.directive('mapp', Map);


	/** @ngInject */
	function Map() {
		return {
			// name: '',
			// priority: 1,
			// terminal: true,
			scope: {}, // {} = isolate, true = child, false/undefined = no change
			bindToController: {
				markers: '=',
				selected: '='
			},
			controller: 'MapController',
			controllerAs: 'mapCtrl',
			// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
			// restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
			// template: '',
			templateUrl: 'app/components/map/map.html'
			// replace: true,
			// transclude: true,
			// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),

			/* jshint unused: false */
			// link: function($scope, el, attr, controller) {}
		};
	}
})();