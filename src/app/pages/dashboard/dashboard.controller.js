(function() {
	'use strict';

	/**
	 * DashboardController
	 *
	 * Description
	 */

	angular
		.module('dashboardPage')
		.controller('DashboardController', DashboardController);

	/** @ngInject */
	function DashboardController($q, $scope, CityEventService) {
		
		var vm = this;

		CityEventService.getEventsList().then(function(response) {
			vm.cityEvents = response;
		}, function(reject) {
			$q.reject(reject);
		});

		$scope.$on('item:clicked', function(event, item) {
			vm.selectedEvent = item;
		});
	}
})();
