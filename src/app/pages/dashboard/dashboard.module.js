(function() {
	'use strict';


	/**
	* dashboardPage Module
	*
	* Description
	*/
	angular
		.module('dashboardPage', [
			'map',
			'list',
			'cityEvent'
		]);
})();